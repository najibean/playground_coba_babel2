import http from 'http' //no need to install, already bawaan node

const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.end('Hello World\n')
}).listen(1338, () => {
    console.log('server running on port ' + 1338)
})

export default server